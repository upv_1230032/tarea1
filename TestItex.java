/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testitex;

import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.parser.PdfTextExtractor;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.StringTokenizer;

/**
 *
 * @author LuisEnrique
 */
public class TestItex {
 Map<String, Integer> mapa = new HashMap<String, Integer>();
 //separacion de cada paabra de la cadena de string
  public void BuscarP(String texto){
    StringTokenizer st = new StringTokenizer (texto);
    String s2="";
     while (st.hasMoreTokens())
        {
            s2 = st.nextToken();
          Compara(s2);
        }
  }
  //buscar la palabra en el mapa 
  // si existe solo aumentar el contador
  // si no agregarla al mapa
  public void Compara(String palabra){
     //ver si ya existe la palabra en el mapa
      if(mapa.containsKey(palabra)){
          //si existe guardar el valor de la llave para despues aumentarlo
         int aux= mapa.get(palabra);
         mapa.put(palabra, aux+1);
      }
      else{
          //insertar un palabra nueva
          mapa.put(palabra, 1);
      }
      
  }
  // impresion de los resultados 
  public void NumPaabras(){
      System.out.println("Numero de palabras diferentes : "+mapa.size());
      System.out.println("Combinaciones de plabras");
      Iterator it = mapa.keySet().iterator();
      
while(it.hasNext()){
     String key = it.next().toString();
  System.out.println("Palabra: " + key + " -> Repeticiones: " + mapa.get(key));
  
}
      //System.out.println("Mapa"+mapa.entrySet());
  }
    public static void main(String[] args) throws IOException {
        // TODO code application logic here
        String p1="";
       PdfReader obj = new PdfReader("C:\\Users\\LuisEnrique\\Documents\\tarea2.pdf");
       int ndp= obj.getNumberOfPages();
        System.out.println("Numero de paginas :"+ndp);
        //formar una cadena de caracteres con todas las palabras del texto
        for(int i=1; i<ndp; i++){
            p1 += PdfTextExtractor.getTextFromPage(obj, i);
        }
        TestItex t =new TestItex ();
        // enviar la cadena de caracteres completa del texto para separar por palabras
      t.BuscarP(p1);
      //imprecion de resultados
      t.NumPaabras();
        
       // System.out.println("Texto:"+p1);
        }
    
    }
    

